import './App.css';
import Sidebar from './components/sidebar/Sidebar';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import PageUnderConstruction from './pages/PageUnderConstruction';
import RestaurantDetailPage from './pages/RestaurantDetailPage';
import Home from './pages/Home';

function App() {
  return (
    <Router>
      <Sidebar />
      <Switch>
        <Route path='/home' exact component={Home} />
        <Route path='/home/restaurant-details/:restaurantId' exact component={RestaurantDetailPage} />
        <Route path='/orders'  component={PageUnderConstruction} />
        <Route path='/notification'  component={PageUnderConstruction} />
        <Route path='/help-support'  component={PageUnderConstruction} />
        <Route path='/settings'  component={PageUnderConstruction} />
        <Redirect to="/home" />
      </Switch>
    </Router>
  );
}

export default App;
