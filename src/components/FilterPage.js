import React, { useState } from "react";
import { Row, Button } from 'react-bootstrap';
import * as BsIcons from 'react-icons/bs';
import * as AiIcons from 'react-icons/ai';
import * as MdIcons from 'react-icons/md';

const FilterPage = ({ setFilterPopup, setButtonSelected, buttonSelected }) => {
    const [selectedFilterMenuItem, setSeletedFilterMenuItem] = useState(buttonSelected);
    const handleFilterBtn = (e) => {
        console.log("enter here")
        setSeletedFilterMenuItem(e.target.id)
    }
    const handleFilters = () => {
        setButtonSelected(selectedFilterMenuItem);
        setFilterPopup(false);
    }
    return <Row className="filter-page-wrapper">
        <Row className='search-filter-text display-flex'>
            Search filters
            <BsIcons.BsXLg className="card-is-open" style={{ marginTop: "8px" }} size={20} onClick={() => setFilterPopup(false)} />
        </Row>
        <Row className='search-filter-text'>
            Sort by
            <div className="display-flex category-icon-wrap"><AiIcons.AiFillFire style={{ background: "rgba(251, 109, 58, 0.1)", color: "#FB6D3A" }} size={20} /> <Button className="open-filter-btn" id="All">open</Button></div>
        </Row>
        <Row className='search-filter-text'>
            Cuisine
        </Row>

        <Row className='search-filter-text'>
            <div> <Button className={`menu-filter-btn${selectedFilterMenuItem === 'All' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="All">All</Button>
                <Button className={`menu-filter-btn${selectedFilterMenuItem === 'Fast Food' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="Fast Food">Fast Food</Button>
                <Button className={`menu-filter-btn${selectedFilterMenuItem === 'American Food' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="American Food">American food</Button> </div>
            <div><Button className={`menu-filter-btn${selectedFilterMenuItem === 'Pizza' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="Pizza">Pizza</Button>
                <Button className={`menu-filter-btn${selectedFilterMenuItem === 'Asian' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="Asian">Asian</Button>
                <Button className={`menu-filter-btn${selectedFilterMenuItem === 'Dessert' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="Dessert">Dessert</Button>
                <Button className={`menu-filter-btn${selectedFilterMenuItem === 'Mexican' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="Mexican">Mexican</Button></div>
            <div><Button className={`menu-filter-btn${selectedFilterMenuItem === 'Breakfast' ? ' activeFilterMenuBtn' : ''}`} onClick={handleFilterBtn} id="Breakfast">Breakfast</Button></div>
            <div className="display-flex store-icon-wrap"><Button className="open-filter-btn">See More...</Button> <div className="more-dropdown"> <MdIcons.MdOutlineKeyboardArrowDown size={30} style={{color:"#626264"}}/></div></div>
        </Row>
        <Row className="content-center">
            <Button className="apply-filters-btn" onClick={handleFilters}>Apply Filters</Button>
        </Row>
    </Row>
}

export default FilterPage;