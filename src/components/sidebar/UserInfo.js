import React from 'react';
import * as BiIcons from 'react-icons/bi';
const UserInfo = () => {
    return (
        <div className='display-flex user-name-wrapper'>
            <div>
                <div className='user-name'>
                    Mark Clarke
                </div>
                <div className='user-email'>markclarke@gmail.com</div>
            </div>
            <BiIcons.BiCode className='sort-icon-user' size={25} />
        </div>
    )
}

export default UserInfo;