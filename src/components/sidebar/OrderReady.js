import React from "react";
import { Button } from 'react-bootstrap';
import * as BsIcons from 'react-icons/bs';

const OrderReady = () => {
    return (
        <div className='user-cart-details'>
            <div className="cross-icon"><BsIcons.BsX size={30} /></div>
            <div className="content-center clock-icon"><BsIcons.BsClock style={{ color: "#FB6D3A", background: "rgba(251, 109, 58, 0.1)" }} size={25} /></div>
            <div className="order-ready">
                Your Order is now Ready
            </div>

            <div className="order-ready-middle-section-text">
                Splint Doumo
            </div>
            <div className="order-ready-middle-section-text">
                Order Id: #ED564F
            </div>
            <div className="content-center">
                <Button className="user-info-details content-center">
                    <div className="detail-text">Details</div> <div className="arrow-right-icon"> <BsIcons.BsArrowRightShort style={{ color: '#FFFFFF' }} size={30} /> </div>
                </Button>
            </div>
        </div>
    )
}

export default OrderReady;