import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';

const SidebarLink = styled(NavLink)`
  display: flex;
  color: #e1e9fc;
  justify-content: space-between;
  align-items: center;
  padding: 20px;
  list-style: none;
  height: 45px;
  text-decoration: none;
  font-size: 16px;
  &:active{
    color:red;
  }
`;

const SidebarLabel = styled.span`
  margin-left: 16px;
  color: #626264;
`;


const SubMenu = ({ item }) => {
 
  return (
    <>
      <SidebarLink to={item.path} onClick={item.subNav}
       >
        <div>
          {item.icon}
          <SidebarLabel>{item.title} </SidebarLabel> {item?.extraIcon}
        </div>
      </SidebarLink>
    </>
  );
};

export default SubMenu;
