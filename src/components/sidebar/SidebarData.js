import React from 'react';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';

export const SidebarData = [
  {
    title: 'Home',
    path: '/home',
    icon: <AiIcons.AiOutlineHome />,
  },
  {
    title: 'Orders',
    path: '/orders',
    icon: <IoIcons.IoIosPaper />,
  },
  {
    title: 'Notification',
    path: '/notification',
    icon: <AiIcons.AiOutlineMail />,
    extraIcon: <span className='circle'>2</span>
  },
  {
    title: 'Help & Support',
    path: '/help-support',
    icon: <IoIcons.IoIosHelpCircleOutline />
  },
  {
    title: 'Settings',
    path: '/settings',
    icon: <AiIcons.AiOutlineSetting />
  },
];
