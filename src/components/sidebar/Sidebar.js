import React from 'react';
import styled from 'styled-components';
import { SidebarData } from './SidebarData';
import SubMenu from './SubMenu';
import { IconContext } from 'react-icons/lib';
import Promo from "../../logos/path410.png";
import OrderReady from './OrderReady';
import UserInfo from './UserInfo';

const SidebarNav = styled.nav`
  background: #F7F7F7;
  display: flex;
  justify-content: center;
  position: relative;
  top: 0;
  left: 0;
  z-index: 10;
  padding:20px;
  border-radius: 0px 24px 24px 0px;
`;

const SidebarWrap = styled.div`
  width: 200px;
  border-radius: 5px;
`;

const Sidebar = () => {

  return (
    <>
      <IconContext.Provider value={{ color: '#000000' }}>
        <SidebarNav sidebar={true}>
          <SidebarWrap>
            <div>
              <div className="display-flex category-icon-wrap promo-wrapper"> <img src={Promo} alt="promo" className="promo-icon" /> Pomo & co</div>
            </div>
            {SidebarData.map((item, index) => {
              return <SubMenu item={item} key={index} />;
            })}
          <OrderReady />
          <UserInfo />
          </SidebarWrap>
        </SidebarNav>
      </IconContext.Provider>
    </>
  );
};

export default Sidebar;
