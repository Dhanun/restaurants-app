import React, { useEffect, useState } from 'react';
import { getAllRestaurants } from '../services/restaurant-service';
import { Row, Card, Button, Col, Container } from 'react-bootstrap';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import * as BsIcons from 'react-icons/bs';
import * as BiIcons from 'react-icons/bi';
import * as MdIcons from 'react-icons/md';
import * as FaIcons from 'react-icons/fa';
import FilterPage from '../components/FilterPage';
import burgerKing from "../logos/1.png";
import pizzaHut from "../logos/2.png";
import macD from "../logos/3.png";
import carrows from "../logos/5.png";
import Loader from '../common/Spinner';

const CardButtonColor = styled.nav`
  color: ${({ isOpen }) => (isOpen ? '#503E9D' : '#FB6D3A')};
  font-weight: 600;
  padding:2px;
`;

const Home = () => {
  const [allRestroData, setAllRestroData] = useState([]);
  const [buttonSelected, setButtonSelected] = useState('All');
  const [openFilterPopup, setFilterPopup] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const history = useHistory();
  useEffect(() => {
    setIsLoading(true);
    fetchData();
    setIsLoading(false);
  }, []);
  const fetchData = async () => {
    try {
      const { allRestaurants } = await getAllRestaurants();
      setAllRestroData(allRestaurants);
    } catch (err) {
      console.log("Error occured while fetching all Restaurants data", err);
    }
  }

  const redirectToRestaurantDetailPage = (restaurantId) => {
    history.push(`/home/restaurant-details/${restaurantId}`)
  }

  const processRestaurantData = (id) => {
    const outputData = [];
    if (id === 'All') {
      return allRestroData;
    } else {
      allRestroData.forEach((item) => {
        const parseStrCuisine = JSON.parse(item.restaurantCuisine);
        const parseStrCategory = JSON.parse(item.restaurantCategory);
        const combineCategory = [...parseStrCuisine, ...parseStrCategory].filter((item, index) => item.indexOf(index) === -1);
        const str = combineCategory.find((item) => item === id);
        if (str) {
          outputData.push(item);
        }
      })
      return outputData;
    }
  }

  const renderCards = (menuItem) => {
    return menuItem.map((item, index) => {
      return (
        <Col key={item.id + index}>
          <Card style={{ width: '23rem' }} className="card-box currsor-pointer" onClick={() => redirectToRestaurantDetailPage(item.id)}>
            <Card.Img className="card-img" variant="top" src={item.restaurantImage} />
            <Card.Body>
              <Row className="card-title-wrapper">
                <Col>
                  <Card.Title className="b">{item.restaurantName}</Card.Title></Col>
                <Col className="card-is-open">
                  <Button className='card-button'><CardButtonColor isOpen={item.isOpen}>{item.isOpen ? "Open Now" : "Closed"}</CardButtonColor></Button>
                </Col></Row>
              <Card.Text>
                {item.restaurantDescription}
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
      );
    })
  }

  const handleFilterBtn = () => {
    setFilterPopup(true);
  }
  return (
    <>
      {!isLoading ? <Container className="home">
        {
          openFilterPopup && <div className='filter-blur-page'>
            <FilterPage setFilterPopup={setFilterPopup} setButtonSelected={setButtonSelected} buttonSelected={buttonSelected} />
          </div>
        }
        <Row className='nav-wrapper'>
          <div>
            <BsIcons.BsFillArrowLeftSquareFill style={{ color: '#503E9D', borderRadius: "10px" }} size={40} />
          </div>
          <div className='card-is-open display-flex'>
            <div className="display-flex store-icon-wrap" style={{ color: '#182135' }}><MdIcons.MdStore className="store-icon" size={30} /> Da Otto</div>
            <BiIcons.BiCode className="sort-icon" size={20} />
            <div className='search-bar display-flex category-icon-wrap'><BsIcons.BsSearch size={20} /> <input className='input-box-search' placeholder='Search for Restaurants  (Press Enter to search)' /> </div>
            <BsIcons.BsFillFilterSquareFill style={{ background: '#E5E5E5', color: '#503E9D', borderRadius: "5px", marginRight: "10px" }} size={40} onClick={handleFilterBtn} />
            <FaIcons.FaShoppingCart style={{ color: '#E5E5E5', borderRadius: "5px", background: "#FB6D3A", padding: "10px", marginRight: "10px" }} size={40} />
          </div>
        </Row>
        <Row>
          <div className='home-heading'>Category</div>
          <Row className="display-flex">
            <div className="display-flex category-icon-wrap"><img src={carrows} alt="carrows" className="category-icon" /> <Button className="category-btn" id="Baked" >Baked </Button></div>
            <div className="display-flex category-icon-wrap"><img src={burgerKing} alt="burgerKing" className="category-icon" /><Button className="category-btn" id="Sweet" >Sweet </Button></div>
            <div className="display-flex category-icon-wrap"><img src={pizzaHut} alt="pizzaHut" className="category-icon" /><Button className="category-btn" id="Hot Dish" >Hot Dish </Button></div>
            <div className="display-flex category-icon-wrap"><img src={macD} alt="macD" className="category-icon" /><Button className="category-btn" id="Fast Food" >Fast Food</Button></div>
            <div className="display-flex category-icon-wrap"><img src={macD} alt="macD" className="category-icon" /><Button className="category-btn" id="Salads">Salads</Button></div>
          </Row>
        </Row>
        <Row>
          <div className='home-heading'>Restaurants</div>
          <Row className="card-wrapper">
            {renderCards(processRestaurantData(buttonSelected))}
          </Row>
        </Row>
      </Container> : <Loader />}
    </>
  );
};


export default Home;