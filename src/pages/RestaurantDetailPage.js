import React, { useEffect, useState } from "react";
import { getRestaurantDetails, getMenuList } from '../services/restaurant-service';
import { useParams, useHistory } from 'react-router-dom';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import * as AiIcons from 'react-icons/ai';
import * as BsIcons from 'react-icons/bs';
import Loader from '../common/Spinner';

const RestaurantDetailPage = () => {
    const [menuList, setMenuList] = useState([]);
    const [restaurantsDetails, setRestaurantsDetails] = useState(null);
    const [buttonSelected, setButtonSelected] = useState('All');
    const { restaurantId } = useParams();
    const [isLoading, setIsLoading] = useState(false);
    const history = useHistory();

    useEffect(() => {
        setIsLoading(true);
        fetchData();
        setIsLoading(false);
    }, []);
    const fetchData = async () => {
        try {
            const data = await Promise.all([getRestaurantDetails(), getMenuList()]);
            setRestaurantsDetails(processRestaurantDetails(data[0].restaurantDetails));
            setMenuList(data[1].menu);
        } catch (err) {
            console.log("Error occured while fetching all Restaurants data", err);
        }
    }

    const redirectToRestaurantHomePage = () => {
        history.push(`/home`)
    }

    const processRestaurantDetails = (data) => {
        const item = data.find((item) => item.id === Number(restaurantId));
        return item;
    }

    const renderOpeningHours = (openingHourStr) => {
        const splitOpeningHourStr = openingHourStr.split(',');
        return splitOpeningHourStr.map((item, index) => {
            return <div key={index}>{item}</div>
        })
    }

    const processMenuData = (id) => {
        const outputData = [];
        menuList.forEach((item) => {
            const parseStr = JSON.parse(item.restaurantName);
            const str = parseStr.find((item) => item === restaurantsDetails.restaurantName);
            if (str) {
                outputData.push(item);
            }
        })
        if (id === 'All') {
            return outputData;
        } else {
            const menuWiseData = [];
            outputData.forEach((item) => {
                const parseItemCategory = JSON.parse(item.itemCategory);
                const str = parseItemCategory.find((itemParse) => itemParse === id);
                if (str) {
                    menuWiseData.push(item);
                }
            });
            return menuWiseData;
        }
    }

    const handleMenuBtn = (e) => {
        e.preventDefault();
        setButtonSelected(e.target.id);
    }

    const renderMenuList = (menuItems) => {
        return menuItems.map((item, index) => {
            return (
                <Col key={item.id + index}>
                    <Card style={{ width: '23rem', margin: '10px' }} className="card-box" >
                        <Card.Img className="card-img" variant="top" src={item.itemPhoto} alt={item.itemName} />
                        <Card.Body> 
                            <Row className="card-title-wrapper detailed-page-title">
                                <Col className="card-details-title">
                                    {item.itemName}
                                </Col>
                                <Col className="card-doller">
                                    {`£${item?.itemCost}`}
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Col>
            );
        })
    }
 
    const countMenuBaked = processMenuData('Baked');
    const countMenuSweet = processMenuData('Sweet');
    const countMenuHotDish = processMenuData('Hot Dish');
    return (
        <>
            {!isLoading ? <Container className="home">
                <Row className='nav-wrapper'>
                    <div>
                        <BsIcons.BsFillArrowLeftSquareFill style={{ color: '#503E9D', borderRadius: "10px" }} size={40} onClick={redirectToRestaurantHomePage}/>
                    </div>
                    <div className='card-is-open display-flex'>
                        <BsIcons.BsFillCartFill style={{ color: '#E5E5E5', borderRadius: "5px", background: "#FB6D3A", padding: "5px", marginRight: "10px" }} size={40} />
                    </div>
                </Row>
                <Row className="restaurants-details-wrapper">
                    <Col className="restaurants-details-text">
                        <div className="home-heading">{restaurantsDetails?.restaurantName} </div>
                        <div className="restaurants-details-sub-text">
                            <div className="margin-text">{restaurantsDetails?.restaurantDescription}</div>
                            <div className="margin-text"><AiIcons.AiOutlineClockCircle /><div className="padding-text">{restaurantsDetails?.openingHours && renderOpeningHours(restaurantsDetails.openingHours)}</div></div>
                            <div className="margin-text"><AiIcons.AiFillPhone /><div className="padding-text">{restaurantsDetails?.contactNumber}</div></div>
                            <div className="margin-text"><AiIcons.AiOutlineGlobal /> <div className="padding-text">{restaurantsDetails?.websiteUrl}</div></div>
                        </div>
                    </Col>
                    <Col>
                        <Card style={{ width: '35rem', margin: '10px' }} className="card-box">
                            <Card.Img className="restaurants-details-card-img" variant="top" src={restaurantsDetails?.restaurantImage} />
                        </Card>
                    </Col>
                </Row>
                <Row className="centered-box">
                    <hr className="hr-border" />
                </Row>

                <Row>
                    <Button className={`menu-btn${buttonSelected === 'All' ? ' activeBtn' : ''}`} id="All" onClick={(e) => handleMenuBtn(e)}>All </Button>
                    <Button className={`menu-btn${buttonSelected === 'Baked' ? ' activeBtn' : ''}`} id="Baked" onClick={(e) => handleMenuBtn(e)}>Baked {`(${countMenuBaked?.length})`}</Button>
                    <Button className={`menu-btn${buttonSelected === 'Sweet' ? ' activeBtn' : ''}`} id="Sweet" onClick={(e) => handleMenuBtn(e)}>Sweet {`(${countMenuSweet?.length})`}</Button>
                    <Button className={`menu-btn${buttonSelected === 'Hot Dish' ? ' activeBtn' : ''}`} id="Hot Dish" onClick={(e) => handleMenuBtn(e)}>Hot Dish {`(${countMenuHotDish.length})`}</Button>
                </Row>
                <Row className="home-heading">Menu</Row>
                <Row className="card-wrapper">
                    
                    {renderMenuList(processMenuData(buttonSelected))}
                </Row>
            </Container> : <Loader />}
        </>
    )
}
export default RestaurantDetailPage;