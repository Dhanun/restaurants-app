import React from 'react';

const PageUnderConstruction = () => {
  return (
    <div className='home'>
      <h1>Page is under construction</h1>
    </div>
  );
};

export default PageUnderConstruction;
