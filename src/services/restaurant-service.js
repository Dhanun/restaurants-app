const headers = new Headers();
headers.append("Authorization", "Basic 34303304-5475-4d63-9352-0d24ed631b37");

export const getAllRestaurants = async () =>{
	const data = await fetch("https://api.sheety.co/bdcbafbc1f4197dda178b9e69f6ccee9/techAlchemyWebTest1/allRestaurants", {
		method: "GET",
		headers: headers
	})
	.then(response => response.json())
	.then(json => {
		return json;
	});
	return data;
}

export const getRestaurantDetails = async () =>{
	const data = await fetch("https://api.sheety.co/bdcbafbc1f4197dda178b9e69f6ccee9/techAlchemyWebTest1/restaurantDetails", {
		method: "GET",
		headers: headers
	})
	.then(response => response.json())
	.then(json => {
		return json;
	});
	return data;
}

export const getMenuList = async () =>{
	const data = await fetch("https://api.sheety.co/bdcbafbc1f4197dda178b9e69f6ccee9/techAlchemyWebTest1/menu", {
		method: "GET",
		headers: headers
	})
	.then(response => response.json())
	.then(json => {
		return json;
	});
	return data;
}